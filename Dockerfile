FROM golang:alpine AS builder

# Set necessary environmet variables needed for our image
ENV GO111MODULE=on \
    CGO_ENABLED=0 \
    GOOS=linux

# Get grpc/protobuf build tools
#RUN apk add protoc
#RUN go get github.com/golang/protobuf/protoc-gen-go
#RUN go get google.golang.org/grpc/cmd/protoc-gen-go-grpc

# Move to working directory /build
WORKDIR /build

# Copy and download dependency using go mod
#COPY go.mod .
#COPY go.sum .
#RUN go mod download

# Copy the code into the container
COPY . .

# Generate protobuf
#RUN protoc --proto_path=. --go_out=. --go_opt=paths=source_relative \
#        --go-grpc_out=. --go-grpc_opt=paths=source_relative \
#        public/*.proto

# Build the application
RUN go build -o main ./exec

# Move to /dist directory as the place for resulting binary folder
WORKDIR /dist

# Copy binary from build to main folder
RUN cp /build/main .

# Build a small image
FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

COPY --from=builder /dist/main /

COPY app.yaml /

# Command to run
ENTRYPOINT ["/main"]