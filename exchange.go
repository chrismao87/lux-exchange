package lux_exchange

import "context"
import "gitlab.com/chrismao87/lux-exchange/public"

type IOrderUpdate interface {
	Update(order *public.Order)
}

type OrderUpdateFunc func(order *public.Order)

func (o OrderUpdateFunc) Update(order *public.Order) {
	o(order)
}

type IAccountUpdate interface {
	Update(account *public.Account)
}

type AccountUpdateFunc func(account *public.Account)

func (a AccountUpdateFunc) Update(account *public.Account) {
	a(account)
}

type GetOrderOptions struct {
	OrderId         string
	ExternalOrderId string
	Symbol          string
}

type ListOrderOptions struct {
	Symbol string
}

type SymbolInfo struct {
	Symbol      string
	BaseAsset   string
	QuoteAsset  string
	TickSize    string
	MinNotional string
}

type IExchange interface {
	GetAccount(ctx context.Context) (*public.Account, error)

	PlaceOrder(ctx context.Context, order *public.Order) error

	CancelOrder(ctx context.Context, order *public.Order) error

	GetOrder(ctx context.Context, opt GetOrderOptions) (*public.Order, error)

	ListOpenOrders(ctx context.Context, opt ListOrderOptions, filter func(*public.Order) bool) ([]*public.Order, error)

	SubscribeOrderUpdate(IOrderUpdate) string

	UnsubscribeOrderUpdate(id string)

	SubscribeAccountUpdate(IAccountUpdate)

	GetSymbolInfo(ctx context.Context, symbol string) (*SymbolInfo, error)
}
