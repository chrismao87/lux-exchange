// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.26.0
// 	protoc        v3.17.1
// source: public/order.proto

package public

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type OrderStatusType int32

const (
	OrderStatusType_OrderStatusType_UNKNOWN OrderStatusType = 0
	OrderStatusType_NEW                     OrderStatusType = 1
	OrderStatusType_ACCEPTED                OrderStatusType = 2
	OrderStatusType_PARTIALLY_FILLED        OrderStatusType = 3
	OrderStatusType_FILLED                  OrderStatusType = 4
	OrderStatusType_CANCELED                OrderStatusType = 5
	OrderStatusType_PENDING_CANCEL          OrderStatusType = 6
	OrderStatusType_REJECTED                OrderStatusType = 7
	OrderStatusType_EXPIRED                 OrderStatusType = 8
)

// Enum value maps for OrderStatusType.
var (
	OrderStatusType_name = map[int32]string{
		0: "OrderStatusType_UNKNOWN",
		1: "NEW",
		2: "ACCEPTED",
		3: "PARTIALLY_FILLED",
		4: "FILLED",
		5: "CANCELED",
		6: "PENDING_CANCEL",
		7: "REJECTED",
		8: "EXPIRED",
	}
	OrderStatusType_value = map[string]int32{
		"OrderStatusType_UNKNOWN": 0,
		"NEW":                     1,
		"ACCEPTED":                2,
		"PARTIALLY_FILLED":        3,
		"FILLED":                  4,
		"CANCELED":                5,
		"PENDING_CANCEL":          6,
		"REJECTED":                7,
		"EXPIRED":                 8,
	}
)

func (x OrderStatusType) Enum() *OrderStatusType {
	p := new(OrderStatusType)
	*p = x
	return p
}

func (x OrderStatusType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (OrderStatusType) Descriptor() protoreflect.EnumDescriptor {
	return file_public_order_proto_enumTypes[0].Descriptor()
}

func (OrderStatusType) Type() protoreflect.EnumType {
	return &file_public_order_proto_enumTypes[0]
}

func (x OrderStatusType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use OrderStatusType.Descriptor instead.
func (OrderStatusType) EnumDescriptor() ([]byte, []int) {
	return file_public_order_proto_rawDescGZIP(), []int{0}
}

type TimeInForceType int32

const (
	TimeInForceType_TimeInForceType_UNKNOWN TimeInForceType = 0
	TimeInForceType_GTC                     TimeInForceType = 1
	TimeInForceType_IOC                     TimeInForceType = 2
	TimeInForceType_FOK                     TimeInForceType = 3
	TimeInForceType_DAY                     TimeInForceType = 4
)

// Enum value maps for TimeInForceType.
var (
	TimeInForceType_name = map[int32]string{
		0: "TimeInForceType_UNKNOWN",
		1: "GTC",
		2: "IOC",
		3: "FOK",
		4: "DAY",
	}
	TimeInForceType_value = map[string]int32{
		"TimeInForceType_UNKNOWN": 0,
		"GTC":                     1,
		"IOC":                     2,
		"FOK":                     3,
		"DAY":                     4,
	}
)

func (x TimeInForceType) Enum() *TimeInForceType {
	p := new(TimeInForceType)
	*p = x
	return p
}

func (x TimeInForceType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (TimeInForceType) Descriptor() protoreflect.EnumDescriptor {
	return file_public_order_proto_enumTypes[1].Descriptor()
}

func (TimeInForceType) Type() protoreflect.EnumType {
	return &file_public_order_proto_enumTypes[1]
}

func (x TimeInForceType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use TimeInForceType.Descriptor instead.
func (TimeInForceType) EnumDescriptor() ([]byte, []int) {
	return file_public_order_proto_rawDescGZIP(), []int{1}
}

type OrderType int32

const (
	OrderType_OrderType_UNKNOWN OrderType = 0
	OrderType_MARKET            OrderType = 1
	OrderType_LIMIT             OrderType = 2
	OrderType_STOP_LOSS         OrderType = 3
)

// Enum value maps for OrderType.
var (
	OrderType_name = map[int32]string{
		0: "OrderType_UNKNOWN",
		1: "MARKET",
		2: "LIMIT",
		3: "STOP_LOSS",
	}
	OrderType_value = map[string]int32{
		"OrderType_UNKNOWN": 0,
		"MARKET":            1,
		"LIMIT":             2,
		"STOP_LOSS":         3,
	}
)

func (x OrderType) Enum() *OrderType {
	p := new(OrderType)
	*p = x
	return p
}

func (x OrderType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (OrderType) Descriptor() protoreflect.EnumDescriptor {
	return file_public_order_proto_enumTypes[2].Descriptor()
}

func (OrderType) Type() protoreflect.EnumType {
	return &file_public_order_proto_enumTypes[2]
}

func (x OrderType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use OrderType.Descriptor instead.
func (OrderType) EnumDescriptor() ([]byte, []int) {
	return file_public_order_proto_rawDescGZIP(), []int{2}
}

type SideType int32

const (
	SideType_SideType_UNKNOWN SideType = 0
	SideType_BUY              SideType = 1
	SideType_SELL             SideType = 2
)

// Enum value maps for SideType.
var (
	SideType_name = map[int32]string{
		0: "SideType_UNKNOWN",
		1: "BUY",
		2: "SELL",
	}
	SideType_value = map[string]int32{
		"SideType_UNKNOWN": 0,
		"BUY":              1,
		"SELL":             2,
	}
)

func (x SideType) Enum() *SideType {
	p := new(SideType)
	*p = x
	return p
}

func (x SideType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (SideType) Descriptor() protoreflect.EnumDescriptor {
	return file_public_order_proto_enumTypes[3].Descriptor()
}

func (SideType) Type() protoreflect.EnumType {
	return &file_public_order_proto_enumTypes[3]
}

func (x SideType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use SideType.Descriptor instead.
func (SideType) EnumDescriptor() ([]byte, []int) {
	return file_public_order_proto_rawDescGZIP(), []int{3}
}

type Order struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Symbol          string          `protobuf:"bytes,1,opt,name=symbol,proto3" json:"symbol,omitempty"`
	OrderId         string          `protobuf:"bytes,2,opt,name=order_id,json=orderId,proto3" json:"order_id,omitempty"`
	ExternalOrderId string          `protobuf:"bytes,3,opt,name=external_order_id,json=externalOrderId,proto3" json:"external_order_id,omitempty"`
	Price           string          `protobuf:"bytes,4,opt,name=price,proto3" json:"price,omitempty"`
	OrigQty         string          `protobuf:"bytes,5,opt,name=orig_qty,json=origQty,proto3" json:"orig_qty,omitempty"`
	ExecQty         string          `protobuf:"bytes,6,opt,name=exec_qty,json=execQty,proto3" json:"exec_qty,omitempty"`
	Commission      string          `protobuf:"bytes,7,opt,name=commission,proto3" json:"commission,omitempty"`
	CommissionAsset string          `protobuf:"bytes,8,opt,name=commission_asset,json=commissionAsset,proto3" json:"commission_asset,omitempty"`
	Status          OrderStatusType `protobuf:"varint,9,opt,name=status,proto3,enum=public.OrderStatusType" json:"status,omitempty"`
	TimeInForce     TimeInForceType `protobuf:"varint,10,opt,name=time_in_force,json=timeInForce,proto3,enum=public.TimeInForceType" json:"time_in_force,omitempty"`
	OrderType       OrderType       `protobuf:"varint,11,opt,name=order_type,json=orderType,proto3,enum=public.OrderType" json:"order_type,omitempty"`
	Side            SideType        `protobuf:"varint,12,opt,name=side,proto3,enum=public.SideType" json:"side,omitempty"`
	TimeCreated     int64           `protobuf:"varint,13,opt,name=time_created,json=timeCreated,proto3" json:"time_created,omitempty"`
	TimeModified    int64           `protobuf:"varint,14,opt,name=time_modified,json=timeModified,proto3" json:"time_modified,omitempty"`
	Exchange        string          `protobuf:"bytes,15,opt,name=exchange,proto3" json:"exchange,omitempty"`
}

func (x *Order) Reset() {
	*x = Order{}
	if protoimpl.UnsafeEnabled {
		mi := &file_public_order_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Order) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Order) ProtoMessage() {}

func (x *Order) ProtoReflect() protoreflect.Message {
	mi := &file_public_order_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Order.ProtoReflect.Descriptor instead.
func (*Order) Descriptor() ([]byte, []int) {
	return file_public_order_proto_rawDescGZIP(), []int{0}
}

func (x *Order) GetSymbol() string {
	if x != nil {
		return x.Symbol
	}
	return ""
}

func (x *Order) GetOrderId() string {
	if x != nil {
		return x.OrderId
	}
	return ""
}

func (x *Order) GetExternalOrderId() string {
	if x != nil {
		return x.ExternalOrderId
	}
	return ""
}

func (x *Order) GetPrice() string {
	if x != nil {
		return x.Price
	}
	return ""
}

func (x *Order) GetOrigQty() string {
	if x != nil {
		return x.OrigQty
	}
	return ""
}

func (x *Order) GetExecQty() string {
	if x != nil {
		return x.ExecQty
	}
	return ""
}

func (x *Order) GetCommission() string {
	if x != nil {
		return x.Commission
	}
	return ""
}

func (x *Order) GetCommissionAsset() string {
	if x != nil {
		return x.CommissionAsset
	}
	return ""
}

func (x *Order) GetStatus() OrderStatusType {
	if x != nil {
		return x.Status
	}
	return OrderStatusType_OrderStatusType_UNKNOWN
}

func (x *Order) GetTimeInForce() TimeInForceType {
	if x != nil {
		return x.TimeInForce
	}
	return TimeInForceType_TimeInForceType_UNKNOWN
}

func (x *Order) GetOrderType() OrderType {
	if x != nil {
		return x.OrderType
	}
	return OrderType_OrderType_UNKNOWN
}

func (x *Order) GetSide() SideType {
	if x != nil {
		return x.Side
	}
	return SideType_SideType_UNKNOWN
}

func (x *Order) GetTimeCreated() int64 {
	if x != nil {
		return x.TimeCreated
	}
	return 0
}

func (x *Order) GetTimeModified() int64 {
	if x != nil {
		return x.TimeModified
	}
	return 0
}

func (x *Order) GetExchange() string {
	if x != nil {
		return x.Exchange
	}
	return ""
}

var File_public_order_proto protoreflect.FileDescriptor

var file_public_order_proto_rawDesc = []byte{
	0x0a, 0x12, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x2f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x12, 0x06, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x22, 0xa7, 0x04, 0x0a,
	0x05, 0x4f, 0x72, 0x64, 0x65, 0x72, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x79, 0x6d, 0x62, 0x6f, 0x6c,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x79, 0x6d, 0x62, 0x6f, 0x6c, 0x12, 0x19,
	0x0a, 0x08, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x07, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x2a, 0x0a, 0x11, 0x65, 0x78, 0x74,
	0x65, 0x72, 0x6e, 0x61, 0x6c, 0x5f, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x5f, 0x69, 0x64, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0f, 0x65, 0x78, 0x74, 0x65, 0x72, 0x6e, 0x61, 0x6c, 0x4f, 0x72,
	0x64, 0x65, 0x72, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x04,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x6f,
	0x72, 0x69, 0x67, 0x5f, 0x71, 0x74, 0x79, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x6f,
	0x72, 0x69, 0x67, 0x51, 0x74, 0x79, 0x12, 0x19, 0x0a, 0x08, 0x65, 0x78, 0x65, 0x63, 0x5f, 0x71,
	0x74, 0x79, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x65, 0x78, 0x65, 0x63, 0x51, 0x74,
	0x79, 0x12, 0x1e, 0x0a, 0x0a, 0x63, 0x6f, 0x6d, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x6f, 0x6d, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f,
	0x6e, 0x12, 0x29, 0x0a, 0x10, 0x63, 0x6f, 0x6d, 0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x5f,
	0x61, 0x73, 0x73, 0x65, 0x74, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0f, 0x63, 0x6f, 0x6d,
	0x6d, 0x69, 0x73, 0x73, 0x69, 0x6f, 0x6e, 0x41, 0x73, 0x73, 0x65, 0x74, 0x12, 0x2f, 0x0a, 0x06,
	0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x17, 0x2e, 0x70,
	0x75, 0x62, 0x6c, 0x69, 0x63, 0x2e, 0x4f, 0x72, 0x64, 0x65, 0x72, 0x53, 0x74, 0x61, 0x74, 0x75,
	0x73, 0x54, 0x79, 0x70, 0x65, 0x52, 0x06, 0x73, 0x74, 0x61, 0x74, 0x75, 0x73, 0x12, 0x3b, 0x0a,
	0x0d, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x69, 0x6e, 0x5f, 0x66, 0x6f, 0x72, 0x63, 0x65, 0x18, 0x0a,
	0x20, 0x01, 0x28, 0x0e, 0x32, 0x17, 0x2e, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x2e, 0x54, 0x69,
	0x6d, 0x65, 0x49, 0x6e, 0x46, 0x6f, 0x72, 0x63, 0x65, 0x54, 0x79, 0x70, 0x65, 0x52, 0x0b, 0x74,
	0x69, 0x6d, 0x65, 0x49, 0x6e, 0x46, 0x6f, 0x72, 0x63, 0x65, 0x12, 0x30, 0x0a, 0x0a, 0x6f, 0x72,
	0x64, 0x65, 0x72, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x0b, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x11,
	0x2e, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x2e, 0x4f, 0x72, 0x64, 0x65, 0x72, 0x54, 0x79, 0x70,
	0x65, 0x52, 0x09, 0x6f, 0x72, 0x64, 0x65, 0x72, 0x54, 0x79, 0x70, 0x65, 0x12, 0x24, 0x0a, 0x04,
	0x73, 0x69, 0x64, 0x65, 0x18, 0x0c, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x10, 0x2e, 0x70, 0x75, 0x62,
	0x6c, 0x69, 0x63, 0x2e, 0x53, 0x69, 0x64, 0x65, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x73, 0x69,
	0x64, 0x65, 0x12, 0x21, 0x0a, 0x0c, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x63, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x64, 0x18, 0x0d, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0b, 0x74, 0x69, 0x6d, 0x65, 0x43, 0x72,
	0x65, 0x61, 0x74, 0x65, 0x64, 0x12, 0x23, 0x0a, 0x0d, 0x74, 0x69, 0x6d, 0x65, 0x5f, 0x6d, 0x6f,
	0x64, 0x69, 0x66, 0x69, 0x65, 0x64, 0x18, 0x0e, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0c, 0x74, 0x69,
	0x6d, 0x65, 0x4d, 0x6f, 0x64, 0x69, 0x66, 0x69, 0x65, 0x64, 0x12, 0x1a, 0x0a, 0x08, 0x65, 0x78,
	0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x18, 0x0f, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x65, 0x78,
	0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x2a, 0xa4, 0x01, 0x0a, 0x0f, 0x4f, 0x72, 0x64, 0x65, 0x72,
	0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x54, 0x79, 0x70, 0x65, 0x12, 0x1b, 0x0a, 0x17, 0x4f, 0x72,
	0x64, 0x65, 0x72, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73, 0x54, 0x79, 0x70, 0x65, 0x5f, 0x55, 0x4e,
	0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x07, 0x0a, 0x03, 0x4e, 0x45, 0x57, 0x10, 0x01,
	0x12, 0x0c, 0x0a, 0x08, 0x41, 0x43, 0x43, 0x45, 0x50, 0x54, 0x45, 0x44, 0x10, 0x02, 0x12, 0x14,
	0x0a, 0x10, 0x50, 0x41, 0x52, 0x54, 0x49, 0x41, 0x4c, 0x4c, 0x59, 0x5f, 0x46, 0x49, 0x4c, 0x4c,
	0x45, 0x44, 0x10, 0x03, 0x12, 0x0a, 0x0a, 0x06, 0x46, 0x49, 0x4c, 0x4c, 0x45, 0x44, 0x10, 0x04,
	0x12, 0x0c, 0x0a, 0x08, 0x43, 0x41, 0x4e, 0x43, 0x45, 0x4c, 0x45, 0x44, 0x10, 0x05, 0x12, 0x12,
	0x0a, 0x0e, 0x50, 0x45, 0x4e, 0x44, 0x49, 0x4e, 0x47, 0x5f, 0x43, 0x41, 0x4e, 0x43, 0x45, 0x4c,
	0x10, 0x06, 0x12, 0x0c, 0x0a, 0x08, 0x52, 0x45, 0x4a, 0x45, 0x43, 0x54, 0x45, 0x44, 0x10, 0x07,
	0x12, 0x0b, 0x0a, 0x07, 0x45, 0x58, 0x50, 0x49, 0x52, 0x45, 0x44, 0x10, 0x08, 0x2a, 0x52, 0x0a,
	0x0f, 0x54, 0x69, 0x6d, 0x65, 0x49, 0x6e, 0x46, 0x6f, 0x72, 0x63, 0x65, 0x54, 0x79, 0x70, 0x65,
	0x12, 0x1b, 0x0a, 0x17, 0x54, 0x69, 0x6d, 0x65, 0x49, 0x6e, 0x46, 0x6f, 0x72, 0x63, 0x65, 0x54,
	0x79, 0x70, 0x65, 0x5f, 0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x07, 0x0a,
	0x03, 0x47, 0x54, 0x43, 0x10, 0x01, 0x12, 0x07, 0x0a, 0x03, 0x49, 0x4f, 0x43, 0x10, 0x02, 0x12,
	0x07, 0x0a, 0x03, 0x46, 0x4f, 0x4b, 0x10, 0x03, 0x12, 0x07, 0x0a, 0x03, 0x44, 0x41, 0x59, 0x10,
	0x04, 0x2a, 0x48, 0x0a, 0x09, 0x4f, 0x72, 0x64, 0x65, 0x72, 0x54, 0x79, 0x70, 0x65, 0x12, 0x15,
	0x0a, 0x11, 0x4f, 0x72, 0x64, 0x65, 0x72, 0x54, 0x79, 0x70, 0x65, 0x5f, 0x55, 0x4e, 0x4b, 0x4e,
	0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x0a, 0x0a, 0x06, 0x4d, 0x41, 0x52, 0x4b, 0x45, 0x54, 0x10,
	0x01, 0x12, 0x09, 0x0a, 0x05, 0x4c, 0x49, 0x4d, 0x49, 0x54, 0x10, 0x02, 0x12, 0x0d, 0x0a, 0x09,
	0x53, 0x54, 0x4f, 0x50, 0x5f, 0x4c, 0x4f, 0x53, 0x53, 0x10, 0x03, 0x2a, 0x33, 0x0a, 0x08, 0x53,
	0x69, 0x64, 0x65, 0x54, 0x79, 0x70, 0x65, 0x12, 0x14, 0x0a, 0x10, 0x53, 0x69, 0x64, 0x65, 0x54,
	0x79, 0x70, 0x65, 0x5f, 0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x07, 0x0a,
	0x03, 0x42, 0x55, 0x59, 0x10, 0x01, 0x12, 0x08, 0x0a, 0x04, 0x53, 0x45, 0x4c, 0x4c, 0x10, 0x02,
	0x42, 0x2b, 0x5a, 0x29, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x63,
	0x68, 0x72, 0x69, 0x73, 0x6d, 0x61, 0x6f, 0x38, 0x37, 0x2f, 0x6c, 0x75, 0x78, 0x2d, 0x65, 0x78,
	0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x2f, 0x70, 0x75, 0x62, 0x6c, 0x69, 0x63, 0x62, 0x06, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_public_order_proto_rawDescOnce sync.Once
	file_public_order_proto_rawDescData = file_public_order_proto_rawDesc
)

func file_public_order_proto_rawDescGZIP() []byte {
	file_public_order_proto_rawDescOnce.Do(func() {
		file_public_order_proto_rawDescData = protoimpl.X.CompressGZIP(file_public_order_proto_rawDescData)
	})
	return file_public_order_proto_rawDescData
}

var file_public_order_proto_enumTypes = make([]protoimpl.EnumInfo, 4)
var file_public_order_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_public_order_proto_goTypes = []interface{}{
	(OrderStatusType)(0), // 0: public.OrderStatusType
	(TimeInForceType)(0), // 1: public.TimeInForceType
	(OrderType)(0),       // 2: public.OrderType
	(SideType)(0),        // 3: public.SideType
	(*Order)(nil),        // 4: public.Order
}
var file_public_order_proto_depIdxs = []int32{
	0, // 0: public.Order.status:type_name -> public.OrderStatusType
	1, // 1: public.Order.time_in_force:type_name -> public.TimeInForceType
	2, // 2: public.Order.order_type:type_name -> public.OrderType
	3, // 3: public.Order.side:type_name -> public.SideType
	4, // [4:4] is the sub-list for method output_type
	4, // [4:4] is the sub-list for method input_type
	4, // [4:4] is the sub-list for extension type_name
	4, // [4:4] is the sub-list for extension extendee
	0, // [0:4] is the sub-list for field type_name
}

func init() { file_public_order_proto_init() }
func file_public_order_proto_init() {
	if File_public_order_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_public_order_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Order); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_public_order_proto_rawDesc,
			NumEnums:      4,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_public_order_proto_goTypes,
		DependencyIndexes: file_public_order_proto_depIdxs,
		EnumInfos:         file_public_order_proto_enumTypes,
		MessageInfos:      file_public_order_proto_msgTypes,
	}.Build()
	File_public_order_proto = out.File
	file_public_order_proto_rawDesc = nil
	file_public_order_proto_goTypes = nil
	file_public_order_proto_depIdxs = nil
}
