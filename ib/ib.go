package ib

import (
	"context"
	"fmt"
	"github.com/hadrianl/ibapi"
	"github.com/rs/xid"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	exchange "gitlab.com/chrismao87/lux-exchange"
	"gitlab.com/chrismao87/lux-exchange/public"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"math"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	exchangeName             = "IB"
	orderIdMappingCollection = "ib_orderid_map"
	seqCollection            = "seq"
	orderIdSeqName           = "ib_orderid_seq"
	account                  = "U2453523"
)

var (
	orderUpdateFuncs = make(map[string]exchange.IOrderUpdate)
	positions        = make(map[string]string)
	openOrdersMap    = make(map[int64]*OpenOrderType)

	omu sync.Mutex

	openOrderEndChan chan struct{}
	orderStatusChan  = make(chan *OrderStatusType, 1000)
)

type OpenOrderType struct {
	id         int64
	contract   *ibapi.Contract
	order      *ibapi.Order
	orderState *ibapi.OrderState
}

type OrderStatusType struct {
	id     int64
	filled float64
	status string
}

type IB struct {
	client         *ibapi.IbClient
	db             *mongo.Database
	defaultWrapper *ibapi.Wrapper
}

func NewIB(ctx context.Context, gatewayAddr string, db *mongo.Database) (*IB, error) {
	parts := strings.Split(gatewayAddr, ":")
	if len(parts) != 2 {
		return nil, fmt.Errorf("invalid gateway addr: %s", gatewayAddr)
	}

	defaultWrapper := ibapi.Wrapper{}
	ic := ibapi.NewIbClient(NewLuxWrapper(&defaultWrapper))
	// tcp connect with tws or gateway
	// fail if tws or gateway had not yet set the trust IP
	port, _ := strconv.Atoi(parts[1])
	if err := ic.Connect(parts[0], port, 0); err != nil {
		return nil, err
	}
	// handshake with tws or gateway, send handshake protocol to tell tws or gateway the version of client
	// and receive the server version and connection time from tws or gateway.
	// fail if someone else had already connected to tws or gateway with same clientID
	if err := ic.HandShake(); err != nil {
		return nil, err
	}
	ic.ReqAccountUpdates(true, account)
	ic.ReqAccountSummary(ic.GetReqID(), "All", "TotalCashValue")
	ic.ReqPositions()

	// start to send req and receive msg from tws or gateway after this
	if err := ic.Run(); err != nil {
		return nil, err
	}

	ib := &IB{
		client:         ic,
		db:             db,
		defaultWrapper: &defaultWrapper,
	}
	go ib.StartListenOrderUpdate(ctx)
	return ib, nil
}

func (ib *IB) StartListenOrderUpdate(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			return
		case o := <-orderStatusChan:
			c := ib.db.Collection(orderIdMappingCollection)
			for _, fn := range orderUpdateFuncs {
				r := c.FindOne(ctx, bson.M{"reqid": o.id})
				if r.Err() != nil {
					log.WithError(r.Err()).Warning("failed to find reqid mapping")
					continue
				}
				var doc bson.M
				if err := r.Decode(&doc); err != nil {
					log.WithError(r.Err()).Error("failed to decode doc")
					continue
				}
				var orderStatus public.OrderStatusType
				switch o.status {
				case "PendingSubmit":
					orderStatus = public.OrderStatusType_ACCEPTED
				case "PendingCancel":
					orderStatus = public.OrderStatusType_PENDING_CANCEL
				case "PreSubmitted":
					orderStatus = public.OrderStatusType_ACCEPTED
				case "Submitted":
					orderStatus = public.OrderStatusType_ACCEPTED
				case "ApiCancelled":
					orderStatus = public.OrderStatusType_PENDING_CANCEL
				case "Cancelled":
					orderStatus = public.OrderStatusType_CANCELED
				case "Filled":
					orderStatus = public.OrderStatusType_FILLED
				default:
					log.Errorf("unknown order status: %s", o.status)
				}
				if _, ok := doc["order"]; !ok {
					log.WithError(r.Err()).Warning("failed to find order record in reqid mapping")
					continue
				}
				origOrder := doc["order"].(primitive.M)
				// TODO: remove manually set enums.
				order := &public.Order{
					Symbol:          origOrder["symbol"].(string),
					OrderId:         doc["orderid"].(string),
					ExternalOrderId: decimal.NewFromInt(o.id).String(),
					Price:           origOrder["price"].(string),
					OrigQty:         origOrder["origqty"].(string),
					ExecQty:         decimal.NewFromFloat(o.filled).String(),
					Commission:      "0",
					CommissionAsset: "USD",
					Status:          orderStatus,
					TimeInForce:     public.TimeInForceType_GTC,
					OrderType:       public.OrderType_LIMIT,
					Side:            public.SideType(origOrder["side"].(int32)),
					Exchange:        exchangeName,
				}
				fn.Update(order)
			}
		}
	}
}

func (ib *IB) GetAccount(ctx context.Context) (*public.Account, error) {
	res := &public.Account{}
	balanceDict := make(map[string]*public.Balance)
	// TODO: take locked and short position into account.
	for symbol, value := range positions {
		balanceDict[symbol] = &public.Balance{
			Symbol: symbol,
			Locked: "0",
			Free:   value,
		}
	}
	res.BalanceDict = balanceDict
	return res, nil
}

func (ib *IB) PlaceOrder(ctx context.Context, order *public.Order) error {
	c := ib.db.Collection(orderIdMappingCollection)
	s := ib.db.Collection(seqCollection)
	priceD, _ := decimal.NewFromString(order.Price)
	limit, _ := priceD.Float64()
	QtyD, _ := decimal.NewFromString(order.OrigQty)
	qty, _ := QtyD.Float64()

	stmt := bson.M{
		"$inc": bson.M{
			"seq": 1,
		},
	}
	res := s.FindOneAndUpdate(ctx, bson.M{"name": orderIdSeqName}, stmt)
	if res.Err() != nil {
		return res.Err()
	}
	var seq bson.M
	if err := res.Decode(&seq); err != nil {
		return err
	}
	nextOid := seq["seq"].(int64)
	doc := bson.M{
		"reqid":   nextOid,
		"orderid": order.OrderId,
		"order":   order,
	}
	_, err := c.InsertOne(ctx, doc)
	if err != nil {
		return err
	}
	o := ibapi.NewLimitOrder(order.Side.String(), limit, qty)
	o.TIF = "GTC"
	o.OutsideRTH = true
	o.Account = account
	o.AllOrNone = true
	ib.client.PlaceOrder(nextOid, &ibapi.Contract{
		Symbol:          order.Symbol,
		SecurityType:    "STK",
		Exchange:        "SMART",
		Currency:        "USD",
		PrimaryExchange: "ISLAND",
	}, o)
	return nil
}

func (ib *IB) CancelOrder(ctx context.Context, order *public.Order) error {
	c := ib.db.Collection(orderIdMappingCollection)
	res := c.FindOne(ctx, bson.M{"orderid": order.OrderId})
	if res.Err() != nil {
		return res.Err()
	}
	var doc bson.M
	if err := res.Decode(&doc); err != nil {
		return err
	}
	ib.client.CancelOrder(doc["reqid"].(int64))
	return nil
}

func (ib *IB) GetOrder(ctx context.Context, opt exchange.GetOrderOptions) (*public.Order, error) {
	return nil, fmt.Errorf("unimplemented")
}

func (ib *IB) ListOpenOrders(ctx context.Context, opt exchange.ListOrderOptions, filter func(*public.Order) bool) ([]*public.Order, error) {
	omu.Lock()
	defer omu.Unlock()
	c := ib.db.Collection(orderIdMappingCollection)
	openOrderEndChan = make(chan struct{})
	openOrdersMap = make(map[int64]*OpenOrderType)
	ib.client.ReqOpenOrders()
	select {
	case <-openOrderEndChan:
		res := make([]*public.Order, 0)
		for id, o := range openOrdersMap {
			if opt.Symbol != "" && o.contract.Symbol != opt.Symbol {
				continue
			}
			// TODO: Include all status:
			// https://interactivebrokers.github.io/tws-api/interfaceIBApi_1_1EWrapper.html#a17f2a02d6449710b6394d0266a353313
			if o.orderState.Status != "Submitted" && o.orderState.Status != "PendingSubmit" && o.orderState.Status != "PreSubmitted" {
				continue
			}
			r := c.FindOne(ctx, bson.M{"reqid": id})
			if r.Err() != nil {
				if r.Err() == mongo.ErrNoDocuments {
					// Skip if reqid not found in db, maybe a manual order.
					continue
				}
				return nil, r.Err()
			}
			var doc bson.M
			if err := r.Decode(&doc); err != nil {
				return nil, err
			}
			// TODO: remove manually set enums.
			order := &public.Order{
				Symbol:          o.contract.Symbol,
				OrderId:         doc["orderid"].(string),
				ExternalOrderId: "",
				Price:           decimal.NewFromFloat(o.order.LimitPrice).String(),
				OrigQty:         decimal.NewFromFloat(o.order.TotalQuantity).String(),
				ExecQty:         decimal.NewFromFloat(o.order.FilledQuantity).String(),
				Commission:      decimal.NewFromFloat(o.orderState.Commission).String(),
				CommissionAsset: "USD",
				Status:          public.OrderStatusType_ACCEPTED,
				TimeInForce:     public.TimeInForceType_GTC,
				OrderType:       public.OrderType_LIMIT,
				Side:            public.SideType(public.SideType_value[o.order.Action]),
				Exchange:        exchangeName,
			}
			res = append(res, order)
		}
		return res, nil
	case <-time.After(time.Second * 10):
		return nil, fmt.Errorf("timeout to get open orders")
	case <-ctx.Done():
		return nil, fmt.Errorf("gracefully shutdown")
	}
}

func (ib *IB) SubscribeOrderUpdate(fn exchange.IOrderUpdate) string {
	id := xid.New().String()
	orderUpdateFuncs[id] = fn
	return id
}

func (ib *IB) UnsubscribeOrderUpdate(id string) {
	delete(orderUpdateFuncs, id)
}

func (ib *IB) SubscribeAccountUpdate(update exchange.IAccountUpdate) {
	panic("implement me")
}

func (ib *IB) GetSymbolInfo(ctx context.Context, symbol string) (*exchange.SymbolInfo, error) {
	return &exchange.SymbolInfo{
		Symbol:      symbol,
		BaseAsset:   symbol,
		QuoteAsset:  "USD",
		TickSize:    "0.01",
		MinNotional: "1",
	}, nil
}

type LuxWrapper struct {
	wrapper *ibapi.Wrapper
}

func NewLuxWrapper(defaultWrapper *ibapi.Wrapper) *LuxWrapper {
	return &LuxWrapper{wrapper: defaultWrapper}
}

func (w LuxWrapper) TickPrice(reqID int64, tickType int64, price float64, attrib ibapi.TickAttrib) {
	w.wrapper.TickPrice(reqID, tickType, price, attrib)
}

func (w LuxWrapper) TickSize(reqID int64, tickType int64, size int64) {
	w.wrapper.TickSize(reqID, tickType, size)
}

func (w LuxWrapper) OrderStatus(orderID int64, status string, filled float64, remaining float64, avgFillPrice float64, permID int64, parentID int64, lastFillPrice float64, clientID int64, whyHeld string, mktCapPrice float64) {
	orderStatusChan <- &OrderStatusType{
		id:     orderID,
		filled: filled,
		status: status,
	}
	w.wrapper.OrderStatus(orderID, status, filled, remaining, avgFillPrice, permID, parentID, lastFillPrice, clientID, whyHeld, mktCapPrice)
}

func (w LuxWrapper) Error(reqID int64, errCode int64, errString string) {
	w.wrapper.Error(reqID, errCode, errString)
}

func (w LuxWrapper) OpenOrder(orderID int64, contract *ibapi.Contract, order *ibapi.Order, orderState *ibapi.OrderState) {
	if orderState.Commission == math.MaxFloat64 {
		orderState.Commission = 0
	}
	openOrdersMap[orderID] = &OpenOrderType{
		id:         orderID,
		contract:   contract,
		order:      order,
		orderState: orderState,
	}
	w.wrapper.OpenOrder(orderID, contract, order, orderState)
}

func (w LuxWrapper) UpdateAccountValue(tag string, val string, currency string, accName string) {
	w.wrapper.UpdateAccountValue(tag, val, currency, accName)
}

func (w *LuxWrapper) UpdatePortfolio(contract *ibapi.Contract, position float64, marketPrice float64, marketValue float64, averageCost float64, unrealizedPNL float64, realizedPNL float64, accName string) {
	if contract.SecurityType != "STK" {
		return
	}
	positions[contract.Symbol] = decimal.NewFromFloat(position).String()
	w.wrapper.UpdatePortfolio(contract, position, marketPrice, marketValue, averageCost, unrealizedPNL, realizedPNL, accName)
}

func (w LuxWrapper) UpdateAccountTime(accTime time.Time) {
	w.wrapper.UpdateAccountTime(accTime)
}

func (w *LuxWrapper) NextValidID(reqID int64) {
	w.wrapper.NextValidID(reqID)
}

func (w LuxWrapper) ContractDetails(reqID int64, conDetails *ibapi.ContractDetails) {
	w.wrapper.ContractDetails(reqID, conDetails)
}

func (w LuxWrapper) ExecDetails(reqID int64, contract *ibapi.Contract, execution *ibapi.Execution) {
	w.wrapper.ExecDetails(reqID, contract, execution)
}

func (w LuxWrapper) UpdateMktDepth(reqID int64, position int64, operation int64, side int64, price float64, size int64) {
	w.wrapper.UpdateMktDepth(reqID, position, operation, side, price, size)
}

func (w LuxWrapper) UpdateMktDepthL2(reqID int64, position int64, marketMaker string, operation int64, side int64, price float64, size int64, isSmartDepth bool) {
	w.wrapper.UpdateMktDepthL2(reqID, position, marketMaker, operation, side, price, size, isSmartDepth)
}

func (w LuxWrapper) UpdateNewsBulletin(msgID int64, msgType int64, newsMessage string, originExchange string) {
	w.wrapper.UpdateNewsBulletin(msgID, msgType, newsMessage, originExchange)
}

func (w LuxWrapper) ManagedAccounts(accountsList []string) {
	w.wrapper.ManagedAccounts(accountsList)
}

func (w LuxWrapper) ReceiveFA(faData int64, cxml string) {
	w.wrapper.ReceiveFA(faData, cxml)
}

func (w LuxWrapper) HistoricalData(reqID int64, bar *ibapi.BarData) {
	w.wrapper.HistoricalData(reqID, bar)
}

func (w LuxWrapper) HistoricalDataEnd(reqID int64, startDateStr string, endDateStr string) {
	w.wrapper.HistoricalDataEnd(reqID, startDateStr, endDateStr)
}

func (w LuxWrapper) HistoricalDataUpdate(reqID int64, bar *ibapi.BarData) {
	w.wrapper.HistoricalDataUpdate(reqID, bar)
}

func (w LuxWrapper) BondContractDetails(reqID int64, conDetails *ibapi.ContractDetails) {
	w.wrapper.BondContractDetails(reqID, conDetails)
}

func (w LuxWrapper) ScannerParameters(xml string) {
	w.wrapper.ScannerParameters(xml)
}

func (w LuxWrapper) ScannerData(reqID int64, rank int64, conDetails *ibapi.ContractDetails, distance string, benchmark string, projection string, legs string) {
	w.wrapper.ScannerData(reqID, rank, conDetails, distance, benchmark, projection, legs)
}

func (w LuxWrapper) ScannerDataEnd(reqID int64) {
	w.wrapper.ScannerDataEnd(reqID)
}

func (w LuxWrapper) TickOptionComputation(reqID int64, tickType int64, impliedVol float64, delta float64, optPrice float64, pvDiviedn float64, gamma float64, vega float64, theta float64, undPrice float64) {
	w.wrapper.TickOptionComputation(reqID, tickType, impliedVol, delta, optPrice, pvDiviedn, gamma, vega, theta, undPrice)
}

func (w LuxWrapper) TickGeneric(reqID int64, tickType int64, value float64) {
	w.wrapper.TickGeneric(reqID, tickType, value)
}

func (w LuxWrapper) TickString(reqID int64, tickType int64, value string) {
	w.wrapper.TickString(reqID, tickType, value)
}

func (w LuxWrapper) TickEFP(reqID int64, tickType int64, basisPoints float64, formattedBasisPoints string, totalDividends float64, holdDays int64, futureLastTradeDate string, dividendImpact float64, dividendsToLastTradeDate float64) {
	w.wrapper.TickEFP(reqID, tickType, basisPoints, formattedBasisPoints, totalDividends, holdDays, futureLastTradeDate, dividendImpact, dividendsToLastTradeDate)
}

func (w LuxWrapper) CurrentTime(t time.Time) {
	w.wrapper.CurrentTime(t)
}

func (w LuxWrapper) RealtimeBar(reqID int64, time int64, open float64, high float64, low float64, close float64, volume int64, wap float64, count int64) {
	w.wrapper.RealtimeBar(reqID, time, open, high, low, close, volume, wap, count)
}

func (w LuxWrapper) FundamentalData(reqID int64, data string) {
	w.wrapper.FundamentalData(reqID, data)
}

func (w LuxWrapper) ContractDetailsEnd(reqID int64) {
	w.wrapper.ContractDetailsEnd(reqID)
}

func (w LuxWrapper) OpenOrderEnd() {
	if openOrderEndChan != nil {
		close(openOrderEndChan)
		openOrderEndChan = nil
	}
	w.wrapper.OpenOrderEnd()
}

func (w LuxWrapper) AccountDownloadEnd(accName string) {
	w.wrapper.AccountDownloadEnd(accName)
}

func (w LuxWrapper) ExecDetailsEnd(reqID int64) {
	w.wrapper.ExecDetailsEnd(reqID)
}

func (w LuxWrapper) DeltaNeutralValidation(reqID int64, deltaNeutralContract ibapi.DeltaNeutralContract) {
	w.wrapper.DeltaNeutralValidation(reqID, deltaNeutralContract)
}

func (w LuxWrapper) TickSnapshotEnd(reqID int64) {
	w.wrapper.TickSnapshotEnd(reqID)
}

func (w LuxWrapper) MarketDataType(reqID int64, marketDataType int64) {
	w.wrapper.MarketDataType(reqID, marketDataType)
}

func (w LuxWrapper) Position(account string, contract *ibapi.Contract, position float64, avgCost float64) {
	if contract.SecurityType != "STK" {
		return
	}
	positions[contract.Symbol] = decimal.NewFromFloat(position).String()
	w.wrapper.Position(account, contract, position, avgCost)
}

func (w LuxWrapper) PositionEnd() {
	w.wrapper.PositionEnd()
}

func (w LuxWrapper) AccountSummary(reqID int64, account string, tag string, value string, currency string) {
	if tag == "TotalCashValue" {
		positions["USD"] = value
	}
	w.wrapper.AccountSummary(reqID, account, tag, value, currency)
}

func (w LuxWrapper) AccountSummaryEnd(reqID int64) {
	w.wrapper.AccountSummaryEnd(reqID)
}

func (w LuxWrapper) VerifyMessageAPI(apiData string) {
	w.wrapper.VerifyMessageAPI(apiData)
}

func (w LuxWrapper) VerifyCompleted(isSuccessful bool, err string) {
	w.wrapper.VerifyCompleted(isSuccessful, err)
}

func (w LuxWrapper) DisplayGroupList(reqID int64, groups string) {
	w.wrapper.DisplayGroupList(reqID, groups)
}

func (w LuxWrapper) DisplayGroupUpdated(reqID int64, contractInfo string) {
	w.wrapper.DisplayGroupUpdated(reqID, contractInfo)
}

func (w LuxWrapper) VerifyAndAuthMessageAPI(apiData string, xyzChallange string) {
	w.wrapper.VerifyAndAuthMessageAPI(apiData, xyzChallange)
}

func (w LuxWrapper) VerifyAndAuthCompleted(isSuccessful bool, err string) {
	w.wrapper.VerifyAndAuthCompleted(isSuccessful, err)
}

func (w LuxWrapper) PositionMulti(reqID int64, account string, modelCode string, contract *ibapi.Contract, position float64, avgCost float64) {
	w.wrapper.PositionMulti(reqID, account, modelCode, contract, position, avgCost)
}

func (w LuxWrapper) PositionMultiEnd(reqID int64) {
	w.wrapper.PositionMultiEnd(reqID)
}

func (w LuxWrapper) AccountUpdateMulti(reqID int64, account string, modelCode string, tag string, value string, currency string) {
	w.wrapper.AccountUpdateMulti(reqID, account, modelCode, tag, value, currency)
}

func (w LuxWrapper) AccountUpdateMultiEnd(reqID int64) {
	w.wrapper.AccountUpdateMultiEnd(reqID)
}

func (w LuxWrapper) SecurityDefinitionOptionParameter(reqID int64, exchange string, underlyingContractID int64, tradingClass string, multiplier string, expirations []string, strikes []float64) {
	w.wrapper.SecurityDefinitionOptionParameter(reqID, exchange, underlyingContractID, tradingClass, multiplier, expirations, strikes)
}

func (w LuxWrapper) SecurityDefinitionOptionParameterEnd(reqID int64) {
	w.wrapper.SecurityDefinitionOptionParameterEnd(reqID)
}

func (w LuxWrapper) SoftDollarTiers(reqID int64, tiers []ibapi.SoftDollarTier) {
	w.wrapper.SoftDollarTiers(reqID, tiers)
}

func (w LuxWrapper) FamilyCodes(famCodes []ibapi.FamilyCode) {
	w.wrapper.FamilyCodes(famCodes)
}

func (w LuxWrapper) SymbolSamples(reqID int64, contractDescriptions []ibapi.ContractDescription) {
	w.wrapper.SymbolSamples(reqID, contractDescriptions)
}

func (w LuxWrapper) SmartComponents(reqID int64, smartComps []ibapi.SmartComponent) {
	w.wrapper.SmartComponents(reqID, smartComps)
}

func (w LuxWrapper) TickReqParams(tickerID int64, minTick float64, bboExchange string, snapshotPermissions int64) {
	w.wrapper.TickReqParams(tickerID, minTick, bboExchange, snapshotPermissions)
}

func (w LuxWrapper) MktDepthExchanges(depthMktDataDescriptions []ibapi.DepthMktDataDescription) {
	w.wrapper.MktDepthExchanges(depthMktDataDescriptions)
}

func (w LuxWrapper) HeadTimestamp(reqID int64, headTimestamp string) {
	w.wrapper.HeadTimestamp(reqID, headTimestamp)
}

func (w LuxWrapper) TickNews(tickerID int64, timeStamp int64, providerCode string, articleID string, headline string, extraData string) {
	w.wrapper.TickNews(tickerID, timeStamp, providerCode, articleID, headline, extraData)
}

func (w LuxWrapper) NewsProviders(newsProviders []ibapi.NewsProvider) {
	w.wrapper.NewsProviders(newsProviders)
}

func (w LuxWrapper) NewsArticle(reqID int64, articleType int64, articleText string) {
	w.wrapper.NewsArticle(reqID, articleType, articleText)
}

func (w LuxWrapper) HistoricalNews(reqID int64, time string, providerCode string, articleID string, headline string) {
	w.wrapper.HistoricalNews(reqID, time, providerCode, articleID, headline)
}

func (w LuxWrapper) HistoricalNewsEnd(reqID int64, hasMore bool) {
	w.wrapper.HistoricalNewsEnd(reqID, hasMore)
}

func (w LuxWrapper) HistogramData(reqID int64, histogram []ibapi.HistogramData) {
	w.wrapper.HistogramData(reqID, histogram)
}

func (w LuxWrapper) RerouteMktDataReq(reqID int64, contractID int64, exchange string) {
	w.wrapper.RerouteMktDataReq(reqID, contractID, exchange)
}

func (w LuxWrapper) RerouteMktDepthReq(reqID int64, contractID int64, exchange string) {
	w.wrapper.RerouteMktDepthReq(reqID, contractID, exchange)
}

func (w LuxWrapper) MarketRule(marketRuleID int64, priceIncrements []ibapi.PriceIncrement) {
	w.wrapper.MarketRule(marketRuleID, priceIncrements)
}

func (w LuxWrapper) Pnl(reqID int64, dailyPnL float64, unrealizedPnL float64, realizedPnL float64) {
	w.wrapper.Pnl(reqID, dailyPnL, unrealizedPnL, realizedPnL)
}

func (w LuxWrapper) PnlSingle(reqID int64, position int64, dailyPnL float64, unrealizedPnL float64, realizedPnL float64, value float64) {
	w.wrapper.PnlSingle(reqID, position, dailyPnL, unrealizedPnL, realizedPnL, value)
}

func (w LuxWrapper) HistoricalTicks(reqID int64, ticks []ibapi.HistoricalTick, done bool) {
	w.wrapper.HistoricalTicks(reqID, ticks, done)
}

func (w LuxWrapper) HistoricalTicksBidAsk(reqID int64, ticks []ibapi.HistoricalTickBidAsk, done bool) {
	w.wrapper.HistoricalTicksBidAsk(reqID, ticks, done)
}

func (w LuxWrapper) HistoricalTicksLast(reqID int64, ticks []ibapi.HistoricalTickLast, done bool) {
	w.wrapper.HistoricalTicksLast(reqID, ticks, done)
}

func (w LuxWrapper) TickByTickAllLast(reqID int64, tickType int64, time int64, price float64, size int64, tickAttribLast ibapi.TickAttribLast, exchange string, specialConditions string) {
	w.wrapper.TickByTickAllLast(reqID, tickType, time, price, size, tickAttribLast, exchange, specialConditions)
}

func (w LuxWrapper) TickByTickBidAsk(reqID int64, time int64, bidPrice float64, askPrice float64, bidSize int64, askSize int64, tickAttribBidAsk ibapi.TickAttribBidAsk) {
	w.wrapper.TickByTickBidAsk(reqID, time, bidPrice, askPrice, bidSize, askSize, tickAttribBidAsk)
}

func (w LuxWrapper) TickByTickMidPoint(reqID int64, time int64, midPoint float64) {
	w.wrapper.TickByTickMidPoint(reqID, time, midPoint)
}

func (w LuxWrapper) OrderBound(reqID int64, apiClientID int64, apiOrderID int64) {
	w.wrapper.OrderBound(reqID, apiClientID, apiOrderID)
}

func (w LuxWrapper) CompletedOrder(contract *ibapi.Contract, order *ibapi.Order, orderState *ibapi.OrderState) {
	w.wrapper.CompletedOrder(contract, order, orderState)
}

func (w LuxWrapper) CompletedOrdersEnd() {
	w.wrapper.CompletedOrdersEnd()
}

func (w LuxWrapper) CommissionReport(commissionReport ibapi.CommissionReport) {
	w.wrapper.CommissionReport(commissionReport)
}

func (w *LuxWrapper) ConnectAck() {
	w.wrapper.ConnectAck()
}

func (w *LuxWrapper) ConnectionClosed() {
	w.wrapper.ConnectionClosed()
}

func (w LuxWrapper) ReplaceFAEnd(reqID int64, text string) {
	w.wrapper.ReplaceFAEnd(reqID, text)
}
