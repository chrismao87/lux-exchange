package binance

import (
	"fmt"
	"github.com/adshao/go-binance/v2"
	"gitlab.com/chrismao87/lux-exchange/public"
)

func updateOrderStatus(status binance.OrderStatusType, order *public.Order) error {
	switch status {
	case binance.OrderStatusTypeNew:
		order.Status = public.OrderStatusType_ACCEPTED
	case binance.OrderStatusTypeFilled:
		order.Status = public.OrderStatusType_FILLED
	case binance.OrderStatusTypePartiallyFilled:
		order.Status = public.OrderStatusType_PARTIALLY_FILLED
	case binance.OrderStatusTypeCanceled:
		order.Status = public.OrderStatusType_CANCELED
	case binance.OrderStatusTypePendingCancel:
		order.Status = public.OrderStatusType_PENDING_CANCEL
	case binance.OrderStatusTypeRejected:
		order.Status = public.OrderStatusType_REJECTED
	case binance.OrderStatusTypeExpired:
		order.Status = public.OrderStatusType_EXPIRED
	default:
		return fmt.Errorf("unknown order status: %s", status)
	}
	return nil
}

func updateTimeInForce(timeInForce binance.TimeInForceType, order *public.Order) error {
	switch timeInForce {
	case binance.TimeInForceTypeGTC:
		order.TimeInForce = public.TimeInForceType_GTC
	case binance.TimeInForceTypeFOK:
		order.TimeInForce = public.TimeInForceType_FOK
	case binance.TimeInForceTypeIOC:
		order.TimeInForce = public.TimeInForceType_IOC
	default:
		return fmt.Errorf("unknown timeInForce: %s", timeInForce)
	}
	return nil
}

func updateOrderType(orderType binance.OrderType, order *public.Order) error {
	switch orderType {
	case binance.OrderTypeMarket:
		order.OrderType = public.OrderType_MARKET
	case binance.OrderTypeLimit:
		order.OrderType = public.OrderType_LIMIT
	case binance.OrderTypeStopLoss:
		order.OrderType = public.OrderType_STOP_LOSS
	default:
		return fmt.Errorf("unknown orderType: %s", orderType)
	}
	return nil
}

func updateSide(side binance.SideType, order *public.Order) error {
	switch side {
	case binance.SideTypeBuy:
		order.Side = public.SideType_BUY
	case binance.SideTypeSell:
		order.Side = public.SideType_SELL
	default:
		return fmt.Errorf("unknown order side: %s", side)
	}
	return nil
}
