package binance

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/adshao/go-binance/v2"
	"github.com/rs/xid"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	exchange "gitlab.com/chrismao87/lux-exchange"
	"gitlab.com/chrismao87/lux-exchange/public"
	"strconv"
	"sync"
	"time"
)

const (
	listenKeyKeepAliveMinute              = 10
	websocketServiceRestartCoolDownMinute = 5
	exchangeName                          = "BINANCE"
)

type Binance struct {
	apiKey             string
	apiSecret          string
	listenKey          string
	client             *binance.Client
	orderUpdateFuncs   map[string]exchange.IOrderUpdate
	accountUpdateFuncs []exchange.IAccountUpdate
	mu                 sync.Mutex
}

func (b *Binance) GetOrder(ctx context.Context, opt exchange.GetOrderOptions) (*public.Order, error) {
	if opt.Symbol == "" || opt.OrderId == "" {
		return nil, fmt.Errorf("invalid GetOrderOptions")
	}
	if binanceOrder, err := b.client.NewGetOrderService().Symbol(opt.Symbol).OrigClientOrderID(opt.OrderId).Do(ctx); err == nil {
		order := &public.Order{
			Symbol:          binanceOrder.Symbol,
			OrderId:         binanceOrder.ClientOrderID,
			ExternalOrderId: strconv.FormatInt(binanceOrder.OrderID, 10),
			Price:           binanceOrder.Price,
			OrigQty:         binanceOrder.OrigQuantity,
			ExecQty:         binanceOrder.ExecutedQuantity,
			TimeCreated:     binanceOrder.Time,
			TimeModified:    binanceOrder.UpdateTime,
		}
		err = updateSide(binanceOrder.Side, order)
		err = updateOrderStatus(binanceOrder.Status, order)
		err = updateOrderType(binanceOrder.Type, order)
		err = updateTimeInForce(binanceOrder.TimeInForce, order)
		if err != nil {
			return nil, err
		}
		return order, nil
	} else {
		return nil, err
	}
}

func (b *Binance) ListOpenOrders(ctx context.Context, opt exchange.ListOrderOptions, filter func(*public.Order) bool) ([]*public.Order, error) {
	if opt.Symbol == "" {
		return nil, fmt.Errorf("invalid ListOrderOptions")
	}
	binanceOrders, err := b.client.NewListOpenOrdersService().Symbol(opt.Symbol).Do(ctx)
	if err != nil {
		return nil, err
	}
	var orders []*public.Order
	for _, binanceOrder := range binanceOrders {
		order := &public.Order{
			Symbol:          binanceOrder.Symbol,
			OrderId:         binanceOrder.ClientOrderID,
			ExternalOrderId: strconv.FormatInt(binanceOrder.OrderID, 10),
			Price:           binanceOrder.Price,
			OrigQty:         binanceOrder.OrigQuantity,
			ExecQty:         binanceOrder.ExecutedQuantity,
			TimeCreated:     binanceOrder.Time,
			TimeModified:    binanceOrder.UpdateTime,
		}
		err = updateSide(binanceOrder.Side, order)
		err = updateOrderStatus(binanceOrder.Status, order)
		err = updateOrderType(binanceOrder.Type, order)
		err = updateTimeInForce(binanceOrder.TimeInForce, order)
		if err != nil {
			return nil, err
		}
		if filter == nil || filter(order) {
			orders = append(orders, order)
		}
	}
	return orders, nil
}

func (b *Binance) CancelOrder(ctx context.Context, order *public.Order) error {
	updatedOrder, err := b.client.NewCancelOrderService().Symbol(order.Symbol).OrigClientOrderID(order.OrderId).Do(ctx)
	if err != nil {
		return err
	}
	order.TimeModified = updatedOrder.TransactTime
	return updateOrderStatus(updatedOrder.Status, order)
}

func (b *Binance) GetAccount(ctx context.Context) (*public.Account, error) {
	if res, err := b.client.NewGetAccountService().Do(ctx); err == nil {
		acc := &public.Account{
			BalanceDict: make(map[string]*public.Balance),
		}
		for _, bal := range res.Balances {
			acc.BalanceDict[bal.Asset] = &public.Balance{
				Symbol: bal.Asset,
				Locked: bal.Locked,
				Free:   bal.Free,
			}
		}
		return acc, nil
	} else {
		return nil, err
	}
}

func (b *Binance) SubscribeOrderUpdate(fn exchange.IOrderUpdate) string {
	b.mu.Lock()
	defer b.mu.Unlock()
	id := xid.New().String()
	b.orderUpdateFuncs[id] = fn
	return id
}

func (b *Binance) UnsubscribeOrderUpdate(id string) {
	b.mu.Lock()
	defer b.mu.Unlock()
	delete(b.orderUpdateFuncs, id)
}

func (b *Binance) SubscribeAccountUpdate(fn exchange.IAccountUpdate) {
	b.accountUpdateFuncs = append(b.accountUpdateFuncs, fn)
}

func (b *Binance) PlaceOrder(ctx context.Context, order *public.Order) error {
	var (
		symbol      string = order.Symbol
		side        binance.SideType
		orderType   binance.OrderType
		timeInForce binance.TimeInForceType
		qty         string = order.OrigQty
		price       string = order.Price
	)
	switch order.Side {
	case public.SideType_BUY:
		side = binance.SideTypeBuy
	case public.SideType_SELL:
		side = binance.SideTypeSell
	default:
		return fmt.Errorf("unsupported order side: %s", order.Side)
	}
	switch order.OrderType {
	case public.OrderType_LIMIT:
		orderType = binance.OrderTypeLimit
	case public.OrderType_MARKET:
		orderType = binance.OrderTypeMarket
	case public.OrderType_STOP_LOSS:
		orderType = binance.OrderTypeStopLoss
	default:
		return fmt.Errorf("unsupported order type: %s", order.OrderType.String())
	}
	switch order.TimeInForce {
	case public.TimeInForceType_GTC:
		timeInForce = binance.TimeInForceTypeGTC
	case public.TimeInForceType_IOC:
		timeInForce = binance.TimeInForceTypeIOC
	case public.TimeInForceType_FOK:
		timeInForce = binance.TimeInForceTypeFOK
	default:
		return fmt.Errorf("unsupported order timeInForce: %s", order.TimeInForce)
	}

	binanceOrder, err := b.client.NewCreateOrderService().
		Symbol(symbol).
		Side(side).
		Type(orderType).
		TimeInForce(timeInForce).
		Quantity(qty).
		Price(price).
		NewClientOrderID(order.OrderId).
		Do(ctx)

	if err != nil {
		return err
	}
	order.TimeCreated = binanceOrder.TransactTime
	order.ExternalOrderId = strconv.FormatInt(binanceOrder.OrderID, 10)
	order.Price = binanceOrder.Price
	order.OrigQty = binanceOrder.OrigQuantity
	order.ExecQty = binanceOrder.ExecutedQuantity
	err = updateSide(binanceOrder.Side, order)
	err = updateOrderStatus(binanceOrder.Status, order)
	err = updateOrderType(binanceOrder.Type, order)
	err = updateTimeInForce(binanceOrder.TimeInForce, order)
	if err != nil {
		return err
	}
	return nil
}

func (b *Binance) GetSymbolInfo(ctx context.Context, symbol string) (*exchange.SymbolInfo, error) {
	res, err := b.client.NewExchangeInfoService().Symbol(symbol).Do(ctx)
	if err != nil {
		return nil, err
	}
	if len(res.Symbols) != 1 {
		return nil, fmt.Errorf("symbol info size is %d", len(res.Symbols))
	}
	s := res.Symbols[0]
	info := &exchange.SymbolInfo{
		Symbol:     s.Symbol,
		BaseAsset:  s.BaseAsset,
		QuoteAsset: s.QuoteAsset,
	}
	for _, filter := range s.Filters {
		if t, ok := filter["tickSize"]; ok {
			info.TickSize = t.(string)
		}
		if t, ok := filter["minNotional"]; ok {
			info.MinNotional = t.(string)
		}
	}
	return info, nil
}

type WsOrderEvent struct {
	Event                          string `json:"e"`
	Time                           int64  `json:"E"`
	Symbol                         string `json:"s"`
	Side                           string `json:"S"`
	ClientOrderId                  string `json:"c"`
	OrderType                      string `json:"o"`
	TimeInForce                    string `json:"f"`
	OrigQuantity                   string `json:"q"`
	OrigPrice                      string `json:"p"`
	StopPrice                      string `json:"P"`
	IcebergQty                     string `json:"F"`
	OrderListId                    int64  `json:"g"`
	OrigClientOrderId              string `json:"C"`
	CurrentExecutionType           string `json:"x"`
	CurrentOrderStatus             string `json:"X"`
	OrderRejectedReason            string `json:"r"`
	OrderId                        int64  `json:"i"`
	LastExecutedQty                string `json:"z"`
	LastExecutedPrice              string `json:"L"`
	Commission                     string `json:"n"`
	CommissionAsset                string `json:"N"`
	TransactionTime                int64  `json:"T"`
	TradeId                        int64  `json:"t"`
	Ignore                         int64  `json:"I"`
	IsOrderOnBook                  bool   `json:"w"`
	IsMaker                        bool   `json:"m"`
	Placeholder                    bool   `json:"M"`
	OrderCreationTime              int64  `json:"O"`
	CumulativeQuoteAssetTransacted string `json:"Z"`
	LastQuoteAssetTransacted       string `json:"Y"`
	QuoteOrderQty                  string `json:"Q"`
}

// https://github.com/binance/binance-spot-api-docs/blob/master/user-data-stream.md
func (b *Binance) startWsUserDataServe(ctx context.Context) {
	errorC := make(chan struct{})
	defer close(errorC)
	startFn := func() (stopC chan struct{}, err error) {
		log.Info("start wsUserDataServe")
		wsHandler := func(message []byte) {
			var result map[string]interface{}
			err := json.Unmarshal(message, &result)
			if err != nil {
				log.WithField("message", string(message)).Error("error unmarshal ws user data response")
				return
			}
			switch result["e"].(string) {
			case "outboundAccountPosition":
				acc := &public.Account{
					BalanceDict: make(map[string]*public.Balance),
				}
				bals := result["B"].([]interface{})
				for _, bal := range bals {
					bal := bal.(map[string]interface{})
					acc.BalanceDict[bal["a"].(string)] = &public.Balance{
						Symbol: bal["a"].(string),
						Locked: bal["l"].(string),
						Free:   bal["f"].(string),
					}
					for _, fn := range b.accountUpdateFuncs {
						go fn.Update(acc)
					}
				}
			case "executionReport":
				event := new(WsOrderEvent)
				err := json.Unmarshal(message, event)
				if err != nil {
					log.WithField("message", message).WithError(err).Error("failed to unmarshal")
					return
				}
				log.WithField("event", event).Info("receive event")
				o := &public.Order{
					Symbol:          event.Symbol,
					Price:           event.OrigPrice,
					Side:            public.SideType(public.SideType_value[event.Side]),
					OrigQty:         event.OrigQuantity,
					ExecQty:         event.LastExecutedQty,
					Commission:      event.Commission,
					CommissionAsset: event.CommissionAsset,
					ExternalOrderId: decimal.NewFromInt(event.OrderId).String(),
					TimeModified:    event.TransactionTime,
					TimeCreated:     event.OrderCreationTime,
					Exchange:        exchangeName,
				}
				if event.OrigClientOrderId == "" {
					o.OrderId = event.ClientOrderId
				} else {
					o.OrderId = event.OrigClientOrderId
				}
				err = updateSide(binance.SideType(event.Side), o)
				err = updateOrderStatus(binance.OrderStatusType(event.CurrentOrderStatus), o)
				err = updateOrderType(binance.OrderType(event.OrderType), o)
				err = updateTimeInForce(binance.TimeInForceType(event.TimeInForce), o)
				b.mu.Lock()
				for _, fn := range b.orderUpdateFuncs {
					go fn.Update(o)
				}
				b.mu.Unlock()
			}
		}
		errHandler := func(err error) {
			log.WithField("error", err).Error("error on handlewsUserData")
			errorC <- struct{}{}
		}

		_, stopC, err = binance.WsUserDataServe(b.listenKey, wsHandler, errHandler)
		return
	}

	stopC, err := startFn()
	if err != nil {
		log.WithFields(log.Fields{
			"listenKey": b.listenKey,
			"error":     err,
		}).Error("error on starting wsUserDataServe")
		return
	}

	for {
		select {
		case <-errorC:
			stopC, err = startFn()
			if err != nil {
				log.WithFields(log.Fields{
					"listenKey": b.listenKey,
					"error":     err,
				}).Error("error on starting wsUserDataServe")
			}
			<-time.After(time.Minute * websocketServiceRestartCoolDownMinute)
		case <-ctx.Done():
			close(stopC)
			log.Info("exit wsUserDataServe stream")
			return
		}
	}
}

func (b *Binance) keepAliveWsUserDataServe(ctx context.Context) {
	log.WithField("listenKeyKeepAliveMinute", listenKeyKeepAliveMinute).Info("start listen key refresh task")
	ticker := time.NewTicker(time.Minute * listenKeyKeepAliveMinute)
	for {
		select {
		case now := <-ticker.C:
			if err := b.client.NewKeepaliveUserStreamService().ListenKey(b.listenKey).Do(ctx); err == nil {
				log.WithField("timestamp", now.Format("2006-01-02 03:04:05")).Info("listenKey refreshed")
			}
		case <-ctx.Done():
			log.Info("gracefully exit keepAliveWsUserDataServe")
			return
		}
	}
}

func NewBinance(ctx context.Context, apiKey string, apiSecret string) (*Binance, error) {
	b := &Binance{
		apiKey:             apiKey,
		apiSecret:          apiSecret,
		client:             binance.NewClient(apiKey, apiSecret),
		orderUpdateFuncs:   make(map[string]exchange.IOrderUpdate),
		accountUpdateFuncs: make([]exchange.IAccountUpdate, 0),
	}
	listenKey, err := b.client.NewStartUserStreamService().Do(ctx)
	if err != nil {
		return nil, err
	}
	b.listenKey = listenKey
	go b.keepAliveWsUserDataServe(ctx)
	go b.startWsUserDataServe(ctx)

	return b, nil
}
