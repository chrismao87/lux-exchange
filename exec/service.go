package main

import (
	"context"
	"fmt"
	"github.com/rs/xid"
	"github.com/sethvargo/go-limiter"
	"github.com/sethvargo/go-limiter/memorystore"
	lux_exchange "gitlab.com/chrismao87/lux-exchange"
	"gitlab.com/chrismao87/lux-exchange/public"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"time"
)

var (
	store limiter.Store
)

func init() {
	store, _ = memorystore.New(&memorystore.Config{
		Tokens:   10,
		Interval: time.Minute,
	})
}

type server struct {
	exchange lux_exchange.IExchange
	public.UnimplementedExchangeServer
}

func (s *server) GetAccount(ctx context.Context, request *public.GetAccountRequest) (*public.GetAccountResponse, error) {
	acc, err := s.exchange.GetAccount(ctx)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.GetAccountResponse{Account: acc}, nil
}

func (s *server) PlaceOrder(ctx context.Context, request *public.PlaceOrderRequest) (*public.PlaceOrderResponse, error) {
	key := fmt.Sprintf("%s-%s", request.GetSymbol(), request.GetPrice())
	_, _, _, ok, _ := store.Take(ctx, key)
	if !ok {
		return nil, status.Error(codes.Internal, fmt.Sprintf("rate limit exceeded on key: %s", key))
	}
	o := &public.Order{
		Symbol:      request.GetSymbol(),
		OrderId:     request.GetOrderId(),
		Price:       request.GetPrice(),
		OrigQty:     request.GetQty(),
		TimeInForce: request.GetTimeInForce(),
		OrderType:   request.GetType(),
		Side:        request.GetSide(),
	}
	if request.GetOrderId() == "" {
		o.OrderId = xid.New().String()
	}
	if err := s.exchange.PlaceOrder(ctx, o); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.PlaceOrderResponse{}, nil
}

func (s *server) CancelOrder(ctx context.Context, request *public.CancelOrderRequest) (*public.CancelOrderResponse, error) {
	o := &public.Order{
		Symbol:  request.GetSymbol(),
		OrderId: request.GetOrderId(),
	}
	if err := s.exchange.CancelOrder(ctx, o); err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.CancelOrderResponse{}, nil
}

func (s *server) GetOrder(ctx context.Context, request *public.GetOrderRequest) (*public.GetOrderResponse, error) {
	opt := lux_exchange.GetOrderOptions{
		OrderId:         request.GetOrderId(),
		ExternalOrderId: request.GetExternalOrderId(),
		Symbol:          request.GetSymbol(),
	}
	o, err := s.exchange.GetOrder(ctx, opt)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.GetOrderResponse{Order: o}, nil
}

func (s *server) ListOpenOrders(ctx context.Context, request *public.ListOpenOrdersRequest) (*public.ListOpenOrdersResponse, error) {
	opt := lux_exchange.ListOrderOptions{
		Symbol: request.GetSymbol(),
	}
	orders, err := s.exchange.ListOpenOrders(ctx, opt, nil)
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.ListOpenOrdersResponse{Orders: orders}, nil
}

func (s *server) GetSymbolInfo(ctx context.Context, request *public.GetSymbolInfoRequest) (*public.GetSymbolInfoResponse, error) {
	info, err := s.exchange.GetSymbolInfo(ctx, request.GetSymbol())
	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &public.GetSymbolInfoResponse{
		Symbol:      info.Symbol,
		BaseAsset:   info.BaseAsset,
		QuoteAsset:  info.QuoteAsset,
		TickSize:    info.TickSize,
		MinNotional: info.MinNotional,
	}, nil
}
