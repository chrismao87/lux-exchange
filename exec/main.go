package main

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	log "github.com/sirupsen/logrus"
	exchange "gitlab.com/chrismao87/lux-exchange"
	"gitlab.com/chrismao87/lux-exchange/alpaca"
	"gitlab.com/chrismao87/lux-exchange/binance"
	"gitlab.com/chrismao87/lux-exchange/ib"
	"gitlab.com/chrismao87/lux-exchange/public"
	"gitlab.com/chrismao87/lux-exchange/util"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"google.golang.org/protobuf/encoding/protojson"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"
)

const (
	DatabaseName = "lux"
)

func init() {

	// Log as JSON instead of the default ASCII formatter.
	log.SetFormatter(&log.JSONFormatter{TimestampFormat: "2006-01-02 15:04:05.000"})

	// Output to stdout instead of the default stderr
	// Can be any io.Writer, see below for File example
	log.SetOutput(os.Stdout)

	//You could set this to any `io.Writer` such as a file
	//file, err := os.OpenFile("/Users/chrismao/Downloads/marin.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	//if err == nil {
	//	log.SetOutput(file)
	//} else {
	//	log.Error("Failed to log to file, using default stderr")
	//}

	// Only log the warning severity or above.
	log.SetLevel(log.InfoLevel)

	//log.SetReportCaller(true)
}

func main() {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.WithError(err).Fatal("failed to load config")
	}
	log.WithFields(log.Fields{
		"exchange":    config.ExchangeName,
		"queue":       config.OrderUpdateQueueName,
		"connectAddr": config.ConnectAddr,
	}).Info("read config")

	logger := log.WithFields(log.Fields{
		"exchange":         config.ExchangeName,
		"orderUpdateQueue": config.OrderUpdateQueueName,
	})

	s, err := session.NewSession(&aws.Config{
		Region: aws.String("us-west-1"),
	})
	if err != nil {
		logger.WithError(err).Error("failed to init aws session")
		return
	}
	svc := sqs.New(s)
	q, err := svc.GetQueueUrl(&sqs.GetQueueUrlInput{
		QueueName: &config.OrderUpdateQueueName,
	})
	if err != nil {
		logger.WithError(err).Fatal("failed to get queue url")
	}

	sendSQS := func(order *public.Order) {
		b, err := protojson.MarshalOptions{
			UseProtoNames:   true,
			UseEnumNumbers:  false,
			EmitUnpopulated: true,
		}.Marshal(order)
		if err != nil {
			logger.WithField("order", order).WithError(err).Error("failed to marshal")
		}
		body := string(b)
		sendMessageInput := &sqs.SendMessageInput{
			MessageBody:    &body,
			QueueUrl:       q.QueueUrl,
			MessageGroupId: &order.Symbol,
		}
		sendMessageOutput, err := svc.SendMessage(sendMessageInput)
		if err != nil {
			logger.WithFields(log.Fields{
				"messageBody": body,
				"QueueUrl":    q.QueueUrl,
			}).WithError(err).Error("failed to send sqs")
			return
		} else {
			logger.WithFields(log.Fields{
				"sendMessageOutput": sendMessageOutput,
				"sendMessageInput":  sendMessageInput,
			}).Info("send sqs ok")
		}
	}

	var instance exchange.IExchange
	ctx, cancel := context.WithCancel(context.Background())
	switch config.ExchangeName {
	case "BINANCE":
		instance, _ = binance.NewBinance(ctx, config.ApiKey, config.ApiSecret)
	case "ALPACA":
		instance, _ = alpaca.NewAlpaca(config.ApiKey, config.ApiSecret, "https://paper-api.alpaca.markets")
	case "IB":
		// Connect mongodb
		mctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		mongoClient, err := mongo.Connect(mctx, options.Client().ApplyURI(config.MongoConnectURL))
		if err != nil {
			log.WithError(err).Fatal("failed to connect mongo")
		}
		db := mongoClient.Database(DatabaseName)
		timeout := time.After(time.Minute * 5)
	L:
		for {
			select {
			case <-timeout:
				log.Fatal("unable to connect to ib after retry, exit")
				return
			default:
				instance, err = ib.NewIB(ctx, config.ConnectAddr, db)
				if err != nil {
					log.WithError(err).Error("failed to connect to ib, attempt to retry")
				} else {
					break L
				}
				<-time.After(time.Second * 10)
			}
		}
	default:
		logger.Fatal("unknown exchange")
	}
	if instance != nil {
		sid := instance.SubscribeOrderUpdate(exchange.OrderUpdateFunc(sendSQS))
		defer instance.UnsubscribeOrderUpdate(sid)
	} else {
		logger.Fatal("no exchange instance found")
	}

	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", config.Port))
	if err != nil {
		logger.WithError(err).Fatal("failed to listen")
	}
	g := grpc.NewServer()
	public.RegisterExchangeServer(g, &server{exchange: instance})
	reflection.Register(g)
	go func() {
		logger.WithField("port", config.Port).Info("start grpc server")
		if err := g.Serve(lis); err != nil {
			logger.WithError(err).Fatal("failed to serve")
		}
	}()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(
		signalChan,
		syscall.SIGHUP,  // kill -SIGHUP XXXX
		syscall.SIGINT,  // kill -SIGINT XXXX or Ctrl+c
		syscall.SIGTERM, // kill -SIGTERM XXXX
		syscall.SIGQUIT, // kill -SIGQUIT XXXX
	)
	<-signalChan
	logger.Info("os.Interrupt - shutting down...")

	cancel()
	g.GracefulStop()

	time.Sleep(time.Second * 10)
}
