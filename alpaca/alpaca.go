package alpaca

import (
	"context"
	"fmt"
	"github.com/alpacahq/alpaca-trade-api-go/alpaca"
	"github.com/alpacahq/alpaca-trade-api-go/common"
	"github.com/alpacahq/alpaca-trade-api-go/stream"
	"github.com/rs/xid"
	"github.com/shopspring/decimal"
	log "github.com/sirupsen/logrus"
	exchange "gitlab.com/chrismao87/lux-exchange"
	"gitlab.com/chrismao87/lux-exchange/public"
	"os"
	"sync"
)

const (
	exchangeName = "ALPACA"
)

type Alpaca struct {
	client           *alpaca.Client
	orderUpdateFuncs map[string]exchange.IOrderUpdate
	mu               sync.Mutex
}

func NewAlpaca(apiKey, apiSecret, baseUrl string) (*Alpaca, error) {
	err := os.Setenv(common.EnvApiKeyID, apiKey)
	err = os.Setenv(common.EnvApiSecretKey, apiSecret)
	if err != nil {
		return nil, err
	}
	alpaca.SetBaseUrl(baseUrl)
	a := &Alpaca{
		client:           alpaca.NewClient(common.Credentials()),
		orderUpdateFuncs: make(map[string]exchange.IOrderUpdate),
	}
	go func() {
		_, err = a.StartStream()
		if err != nil {
			log.WithError(err).Errorf("failed to start stream")
		}
	}()
	return a, nil
}

func (a *Alpaca) StartStream() (stopC chan struct{}, err error) {
	stopC = make(chan struct{})
	handler := func(msg interface{}) {
		tradeupdate := msg.(alpaca.TradeUpdate)
		log.WithField("tradeupdate", tradeupdate).Info("receive trade update")
		o := a.convertAlpacaOrderToOrderProto(&tradeupdate.Order)
		o.Exchange = exchangeName
		for _, fn := range a.orderUpdateFuncs {
			fn.Update(o)
		}
	}
	if err = stream.Register(alpaca.TradeUpdates, handler); err != nil {
		return
	}
	<-stopC
	if err = stream.Deregister(alpaca.TradeUpdates); err != nil {
		return
	}
	return
}

func (a *Alpaca) GetAccount(ctx context.Context) (*public.Account, error) {
	acc, err := a.client.GetAccount()
	if err != nil {
		return nil, err
	}
	positions, err := a.client.ListPositions()
	if err != nil {
		return nil, err
	}
	res := &public.Account{}
	balanceDict := make(map[string]*public.Balance)
	// TODO: take locked and short position into account.
	for _, pos := range positions {
		balanceDict[pos.Symbol] = &public.Balance{
			Symbol: pos.Symbol,
			Locked: "0",
			Free:   pos.Qty.String(),
		}
	}
	balanceDict["USD"] = &public.Balance{
		Symbol: "USD",
		Locked: "0",
		Free:   acc.Cash.String(),
	}
	res.BalanceDict = balanceDict
	return res, nil
}

func (a *Alpaca) PlaceOrder(ctx context.Context, order *public.Order) error {
	qty, _ := decimal.NewFromString(order.OrigQty)
	price, _ := decimal.NewFromString(order.Price)
	req := alpaca.PlaceOrderRequest{
		AssetKey:      &order.Symbol,
		Qty:           qty,
		LimitPrice:    &price,
		ExtendedHours: false,
		ClientOrderID: order.OrderId,
	}
	switch order.Side {
	case public.SideType_BUY:
		req.Side = alpaca.Buy
	case public.SideType_SELL:
		req.Side = alpaca.Sell
	}
	switch order.OrderType {
	case public.OrderType_LIMIT:
		req.Type = alpaca.Limit
	case public.OrderType_MARKET:
		req.Type = alpaca.Market
	default:
		return fmt.Errorf("unsupported order type: %s", order.OrderType.String())
	}
	switch order.TimeInForce {
	case public.TimeInForceType_GTC:
		req.TimeInForce = alpaca.GTC
	case public.TimeInForceType_DAY:
		req.TimeInForce = alpaca.Day
	default:
		return fmt.Errorf("unsupported time_in_force: %s", order.TimeInForce.String())
	}
	if _, err := a.client.PlaceOrder(req); err != nil {
		return err
	}
	return nil
}

func (a *Alpaca) CancelOrder(ctx context.Context, order *public.Order) error {
	o, err := a.client.GetOrderByClientOrderID(order.OrderId)
	if err != nil {
		return err
	}
	if err := a.client.CancelOrder(o.ID); err != nil {
		return err
	}
	return nil
}

func (a *Alpaca) GetOrder(ctx context.Context, opt exchange.GetOrderOptions) (*public.Order, error) {
	o, err := a.client.GetOrderByClientOrderID(opt.OrderId)
	if err != nil {
		return nil, err
	}
	order := a.convertAlpacaOrderToOrderProto(o)
	return order, nil
}

func (a *Alpaca) convertAlpacaOrderToOrderProto(o *alpaca.Order) *public.Order {
	order := &public.Order{
		Symbol:          o.Symbol,
		OrderId:         o.ClientOrderID,
		ExternalOrderId: o.ID,
		OrigQty:         o.Qty.String(),
		ExecQty:         o.FilledQty.String(),
		TimeCreated:     o.CreatedAt.Unix(),
		TimeModified:    o.UpdatedAt.Unix(),
	}
	if o.LimitPrice != nil {
		order.Price = o.LimitPrice.String()
	}

	switch o.Status {
	case "accepted":
		fallthrough
	case "new":
		order.Status = public.OrderStatusType_ACCEPTED
	case "filled":
		order.Status = public.OrderStatusType_FILLED
	case "canceled":
		order.Status = public.OrderStatusType_CANCELED
	case "expired":
		order.Status = public.OrderStatusType_EXPIRED
	case "pending_cancel":
		order.Status = public.OrderStatusType_PENDING_CANCEL
	case "partially_filled":
		order.Status = public.OrderStatusType_PARTIALLY_FILLED
	}
	switch o.TimeInForce {
	case alpaca.GTC:
		order.TimeInForce = public.TimeInForceType_GTC
	case alpaca.Day:
		order.TimeInForce = public.TimeInForceType_DAY
	}
	switch o.Type {
	case alpaca.Limit:
		order.OrderType = public.OrderType_LIMIT
	case alpaca.Market:
		order.OrderType = public.OrderType_MARKET
	}
	switch o.Side {
	case alpaca.Buy:
		order.Side = public.SideType_BUY
	case alpaca.Sell:
		order.Side = public.SideType_SELL
	}
	return order
}

func (a *Alpaca) ListOpenOrders(ctx context.Context, opt exchange.ListOrderOptions, filter func(*public.Order) bool) ([]*public.Order, error) {
	status := "open"
	limit := 500
	nested := false
	orders, err := a.client.ListOrders(&status, nil, &limit, &nested)
	if err != nil {
		return nil, err
	}
	res := make([]*public.Order, 0)
	for _, o := range orders {
		order := a.convertAlpacaOrderToOrderProto(&o)
		res = append(res, order)
	}
	return res, nil
}

func (a *Alpaca) SubscribeOrderUpdate(fn exchange.IOrderUpdate) string {
	a.mu.Lock()
	defer a.mu.Unlock()
	id := xid.New().String()
	a.orderUpdateFuncs[id] = fn
	return id
}

func (a *Alpaca) UnsubscribeOrderUpdate(id string) {
	a.mu.Lock()
	defer a.mu.Unlock()
	delete(a.orderUpdateFuncs, id)
}

func (a *Alpaca) SubscribeAccountUpdate(update exchange.IAccountUpdate) {
	panic("implement me")
}

func (a *Alpaca) GetSymbolInfo(ctx context.Context, symbol string) (*exchange.SymbolInfo, error) {
	return &exchange.SymbolInfo{
		Symbol:      symbol,
		BaseAsset:   symbol,
		QuoteAsset:  "USD",
		TickSize:    "0.01",
		MinNotional: "1",
	}, nil
}
