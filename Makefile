# Reference: https://gist.github.com/mpneuried/0594963ad38e68917ef189b4e6a269db

# import config.
# You can change the default config with `make cnf="config_special.env" build`
cnf ?= config.env
include $(cnf)
export $(shell sed 's/=.*//' $(cnf))

generate:
	protoc --proto_path=. --go_out=. --go_opt=paths=source_relative \
        --go-grpc_out=. --go-grpc_opt=paths=source_relative \
        public/*.proto

build:
	docker build -t $(APP_NAME) .

run:
	docker run -t -t --rm --env-file=./config.env --name="$(APP_NAME)" $(APP_NAME)