package lux_exchange

import (
	"github.com/rs/xid"
	"gitlab.com/chrismao87/lux-exchange/public"
	"time"
)

type NewOrderOption struct {
	Symbol      string
	Price       string
	Quantity    string
	Type        public.OrderType
	Side        public.SideType
	TimeInForce public.TimeInForceType
}

func NewOrder(opt NewOrderOption) *public.Order {
	return &public.Order{
		Symbol:      opt.Symbol,
		OrderId:     xid.New().String(),
		Price:       opt.Price,
		OrigQty:     opt.Quantity,
		Status:      public.OrderStatusType_NEW,
		TimeInForce: opt.TimeInForce,
		OrderType:   opt.Type,
		Side:        opt.Side,
		TimeCreated: time.Now().Unix(),
	}
}
