package util

import "github.com/spf13/viper"

const EnvPrefix = "LUX"

type Config struct {
	ExchangeName         string `mapstructure:"exchange_name"`
	ApiKey               string `mapstructure:"exchange_api_key"`
	ApiSecret            string `mapstructure:"exchange_api_secret"`
	ConnectAddr          string `mapstructure:"exchange_connect_addr"`
	OrderUpdateQueueName string `mapstructure:"order_update_queue_name"`
	MongoConnectURL      string `mapstructure:"mongo_connect_url"`
	Port                 int    `mapstructure:"port"`
}

func LoadConfig(path string) (config Config, err error) {
	viper.SetEnvPrefix(EnvPrefix)
	viper.AddConfigPath(path)
	viper.SetConfigName("app")
	viper.SetConfigType("yaml")
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}
	err = viper.Unmarshal(&config)

	return
}
