module gitlab.com/chrismao87/lux-exchange

go 1.16

require (
	github.com/adshao/go-binance/v2 v2.2.2
	github.com/alpacahq/alpaca-trade-api-go v1.8.2
	github.com/aws/aws-sdk-go v1.38.51
	github.com/hadrianl/ibapi v0.0.0-20210428041841-65ae418d9353
	github.com/rs/xid v1.3.0
	github.com/sethvargo/go-limiter v0.6.0 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	go.mongodb.org/mongo-driver v1.5.3
	go.uber.org/atomic v1.8.0 // indirect
	go.uber.org/multierr v1.7.0 // indirect
	go.uber.org/zap v1.17.0 // indirect
	google.golang.org/grpc v1.38.0
	google.golang.org/protobuf v1.26.0
)
